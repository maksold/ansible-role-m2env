# Ansible Role: Magneto 2 Environment
Installs necessary tools (n98-magerun2, CacheTool) makes some configurations (ulimit) for Magento 2 web servers.

## Requirements

This role require Ansible 2.9 or higher.

This role was designed for:
  - RHEL/CentOS 7/8
  
## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    m2env_n98magerun2_url: 'https://files.magerun.net/n98-magerun2.phar'

Download URL for n98-magerun2 tool.

    m2env_cachetool_url: 'https://gordalina.github.io/cachetool/downloads/cachetool-7.0.0.phar'

Download URL for cachetool.

    magento2_ulimit: []

A list of the ulimit configs.

## Dependencies

No.

## License

MIT / BSD

## Author Information

This role was created in 2020 by [Maksim Soldatjonok](https://www.maksold.com/).